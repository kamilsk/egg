# github.com/Songmu/wrapcommander v0.1.0
github.com/Songmu/wrapcommander
# github.com/davecgh/go-spew v1.1.1
github.com/davecgh/go-spew/spew
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/izumin5210/execx v0.1.0
github.com/izumin5210/execx
# github.com/izumin5210/gex v0.6.1 => github.com/kamilsk/gex v0.6.0-e4
github.com/izumin5210/gex
github.com/izumin5210/gex/pkg/manager
github.com/izumin5210/gex/pkg/manager/dep
github.com/izumin5210/gex/pkg/manager/mod
github.com/izumin5210/gex/pkg/tool
# github.com/pkg/errors v0.8.1
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
github.com/pmezard/go-difflib/difflib
# github.com/spf13/afero v1.2.2
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cobra v1.0.0
github.com/spf13/cobra
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/stretchr/testify v1.5.1
github.com/stretchr/testify/assert
github.com/stretchr/testify/require
# go.octolab.org/toolkit/cli v0.0.9
go.octolab.org/toolkit/cli/cobra
go.octolab.org/toolkit/cli/internal/os/shell
# golang.org/x/sys v0.0.0-20191018095205-727590c5006e
golang.org/x/sys/windows
# golang.org/x/text v0.3.0
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
