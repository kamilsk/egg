module github.com/kamilsk/egg/tools

go 1.11

require (
	github.com/golang/mock v1.4.3
	github.com/golangci/golangci-lint v1.25.1
	golang.org/x/tools v0.3.3
)

replace golang.org/x/tools => github.com/kamilsk/go-tools v0.0.3
