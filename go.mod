module github.com/kamilsk/egg

go 1.11

require (
	github.com/izumin5210/gex v0.6.1
	github.com/spf13/afero v1.2.2
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.5.1
	go.octolab.org/toolkit/cli v0.0.9
)

replace github.com/izumin5210/gex => github.com/kamilsk/gex v0.6.0-e4
